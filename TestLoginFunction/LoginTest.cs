﻿using Task3LoginFunctionality;
using Xunit;

namespace TestLoginFunction
{
    public class LoginTest
    {
        [Fact]
        public void TestToCheckLoginFunc()
        {
            Login login = new Login();
            
            string Username = "Bharti";
            int password = 54321;

            //Initialization
            string expectedResult = "Successfully Login";

            //ActualResult call Dev code
            string actualResult = login.GetUserLogin(Username,password);

            //Assert
            Assert.Equal(expectedResult, actualResult);
        }
    }
}